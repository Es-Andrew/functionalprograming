module Lab02 (take) where

takeItem :: [a] -> Int -> a
takeItem list 1     = head list
takeItem list index = takeItem (tail list) (index - 1)

truncateListFromStart :: [a] -> Int -> [a]
truncateListFromStart list 1 = tail list
truncateListFromStart list index = truncateListFromStart (tail list) (index - 1)

truncateListFromEnd :: [a] -> Int -> [a]
truncateListFromEnd [] _ = []
truncateListFromEnd list number
  | (length list) - number > 0 = head list : truncateListFromEnd (tail list) number
  | otherwise = []

endItem :: [a] -> a
endItem [] = error "empty list"
endItem list
  | length list == 1 = head list
  | otherwise = endItem (tail list)

swap :: [a] -> [a]
swap list = (takeItem list 3) : (takeItem list 2) : (head list) : (truncateListFromStart list 3)

strRoot :: Int -> [Char]
strRoot number = "Корень " ++ (show number) ++ "-ой степени"

list :: a -> a -> a -> a -> [a]
list i1 i2 i3 i4 = i1 : i2 : i3 : i4 : []

triangleSquare :: Float -> Float -> Float -> Float
triangleSquare a b c = let semiperimeter = (a + b + c) / 2 in sqrt (semiperimeter * (semiperimeter - a) * (semiperimeter - b) * (semiperimeter - c))

rightAllSwope :: [a] -> [a]
rightAllSwope []   = []
rightAllSwope list = endItem list : truncateListFromEnd list 1
