%%%-------------------------------------------------------------------
%%% @author Andrew
%%% @doc
%%%
%%% @end
%%% Created : 02. Oct 2017 3:26 PM
%%%-------------------------------------------------------------------
-module(multiLib).
-author("Andrew").

%% API
-export([takeItem/2, takeEndItem/1, truncateListFromStart/2, truncateListFromEnd/2,
  isContains/2, removeItem/2, isFlatList/1, toFlatList/1, foldListToNumber/1, replaceItemInList/3, firstAtomInList/1,
  listCount/1, takeLists/1, maxNumber/2, numberSign/1, reverseList/1, listLength/1, logicalNegation/1,
  logicalEquivalence/2]).

takeItem([], _) -> [];
takeItem(X, 1) -> hd(X);
takeItem(X, Number) -> takeItem(tl(X), Number - 1).

takeEndItem([]) -> [];
takeEndItem(List) when length(List) == 1 -> hd(List);
takeEndItem(List) -> takeEndItem(tl(List)).

truncateListFromStart([], _) -> [];
truncateListFromStart(List, 0) -> List;
truncateListFromStart(List, Number) -> truncateListFromStart(tl(List), Number - 1).

truncateListFromEnd([], _) -> [];
truncateListFromEnd(List, Number) when length(List) - Number =< 0 -> [];
truncateListFromEnd(List, Number) -> [hd(List)] ++ truncateListFromEnd(tl(List), Number).

reverseList(List) -> reverseList(List, []).
reverseList([], Acc) -> Acc;
reverseList(List, Acc) -> reverseList(tl(List), [hd(List)] ++ Acc).

listLength([]) -> 0;
listLength(List) -> 1 + listLength(tl(List)).

isContains([], _) -> false;
isContains(List, Item) when is_list(hd(List)) andalso hd(List) =/= Item ->
  isContains(hd(List), Item) or isContains(tl(List), Item);
isContains(List, Item) -> if hd(List) =:= Item -> true;
                            true -> isContains(tl(List), Item)
                          end.

removeItem([], _) -> [];
removeItem(List, Item) when hd(List) =:= Item -> removeItem(tl(List), Item);
removeItem(List, Item) -> [hd(List)] ++ removeItem(tl(List), Item).

isFlatList([]) -> true;
isFlatList(List) -> if is_list(hd(List)) -> false;
                      true -> isFlatList(tl(List))
                    end.

replaceItemInList([], _, _) -> [];
replaceItemInList(List, OldItem, NewItem) when is_list(hd(List)) andalso hd(List) =/= OldItem ->
  [replaceItemInList(hd(List), OldItem, NewItem)] ++ replaceItemInList(tl(List), OldItem, NewItem);
replaceItemInList(List, OldItem, NewItem) -> if hd(List) =:= OldItem
  -> [NewItem] ++ replaceItemInList(tl(List), OldItem, NewItem);
                                               true ->
                                                 [hd(List)] ++ replaceItemInList(tl(List),
                                                   OldItem, NewItem)
                                             end.

toFlatList([]) -> [];
toFlatList(List) -> if is_list(hd(List)) -> toFlatList(hd(List)) ++ toFlatList(tl(List));
                      true -> [hd(List)] ++ toFlatList(tl(List))
                    end.

firstAtomInList([]) -> false;
firstAtomInList(List) when is_list(hd(List)) -> firstAtomInList(toFlatList(List));
firstAtomInList(List) -> if is_atom(hd(List)) -> hd(List);
                             true -> firstAtomInList(tl(List))
                           end.

foldListToNumber([]) -> 0;
foldListToNumber(List) when is_list(hd(List)) -> foldListToNumber(hd(List)) + foldListToNumber(tl(List));
foldListToNumber(List) -> if is_number(hd(List)) -> hd(List) + foldListToNumber(tl(List));
                            true -> foldListToNumber(tl(List))
                          end.

listCount([]) -> 0;
listCount(List) when is_list(hd(List)) -> 1 + listCount(tl(List));
listCount(List) -> listCount(tl(List)).

takeLists([]) -> [];
takeLists(List) when is_list(hd(List)) -> hd(List) ++ takeLists(tl(List));
takeLists(List) -> takeLists(tl(List)).

maxNumber(N1, N2) -> if N1 > N2 -> N1;
                       true -> N2
                     end.

numberSign(Number) -> if Number >= 0 -> '+';
                        true -> '-'
                      end.

logicalNegation(true) -> false;
logicalNegation(false) -> true.

logicalEquivalence(false, SecondAtom) -> logicalNegation(SecondAtom);
logicalEquivalence(true, SecondAtom) -> SecondAtom.
