-module(lab03).
-export([unknownFunc/0, maxFromThreeNumber/3, numberAndSignList/1, omgFunc/2, isFirstNumberNoLessThanSecond/2]).
-import(multiLib, [takeEndItem/1, takeItem/2, maxNumber/2, numberSign/1]).

unknownFunc() -> unknownFunc([open, close, halph]).
unknownFunc(List) -> [[takeItem(List, 2)] ++ [[takeEndItem(List)]] ++ [hd(List)]].

maxFromThreeNumber(N1, N2, N3) -> maxNumber(maxNumber(N1, N2), N3).

numberAndSignList(Number) -> [Number * 5, numberSign(Number)].

omgFunc(Arg1, Arg2) -> [[Arg1],[Arg2]].

isFirstNumberNoLessThanSecond(N1, N2) -> if N1 >= N2 -> true;
                                           true -> false
                                         end.
