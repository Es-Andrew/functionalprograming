-module(lab04).
-export([fibonacciNumber/1]).

fibonacciNumber(Index) when Index < 2 -> 1;
fibonacciNumber(Index) -> fibonacciNumber(Index, 1, 1).
fibonacciNumber(1, _, FibN1) -> FibN1;
fibonacciNumber(Index, FibN2, FibN1) -> fibonacciNumber(Index - 1, FibN1, FibN2 + FibN1).


%%% fibonacciNumber(Index) when Index < 2 -> 1;
%%% fibonacciNumber(Index) -> fibonacciNumber(Index - 1) + fibonacciNumber(Index - 2).