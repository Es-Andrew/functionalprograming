-module(lab05).
-author("Andrew").

-export([isConsistItemOnLevel/3, listDetails/1, isContains/2, symmetricSubtractSets/2, subtractSets/2, nonStrictContains/2]).
-import(multiLib, [listCount/1, takeLists/1, removeItem/2]).

isConsistItemOnLevel([], _, _) -> false;
isConsistItemOnLevel(List, SubList, Level) -> isConsistItemOnLevel(List, SubList, Level, 1).
isConsistItemOnLevel([], _, _, _) -> false;
isConsistItemOnLevel(_, _, Level, CurLevel) when CurLevel > Level -> false;
isConsistItemOnLevel(List, SubList, Level, CurLevel) when is_list(hd(List)) andalso hd(List) =/= SubList ->
  isConsistItemOnLevel(hd(List), SubList, Level, CurLevel + 1) or isConsistItemOnLevel(tl(List), SubList, Level, CurLevel);
isConsistItemOnLevel(List, SubList, Level, CurLevel) -> if hd(List) =:= SubList andalso CurLevel =:= Level -> true;
                                                          true ->
                                                            isConsistItemOnLevel(tl(List), SubList, Level, CurLevel)
                                                        end.

listDetails(List) -> listDetails(List, 1).
listDetails([], _) -> [];
listDetails(List, Level) -> [[Level, listCount(List)]] ++ listDetails(takeLists(List), Level + 1).

isContains([], _) -> false;
isContains(List, Item) when hd(List) =:= Item -> true;
isContains(List, Item) -> isContains(tl(List), Item).

symmetricSubtractSets(Set1, []) -> Set1;
symmetricSubtractSets([], Set2) -> Set2;
symmetricSubtractSets(Set1, Set2) ->
  case isContains(Set2, hd(Set1)) of
      true -> symmetricSubtractSets(tl(Set1), removeItem(Set2, hd(Set1)));
      false -> [hd(Set1)] ++ symmetricSubtractSets(tl(Set1), Set2)
end.

subtractSets([], _) -> [];
subtractSets(Set1, Set2) ->
  case isContains(Set2, hd(Set1)) of
      true -> subtractSets(tl(Set1), Set2);
      false -> [hd(Set1)] ++ subtractSets(tl(Set1), Set2)
end.


nonStrictContains([], _) -> true;
nonStrictContains(Set1, Set2) ->
  case isContains(Set2, hd(Set1)) of
      true -> nonStrictContains(tl(Set1), Set2);
      false -> false
end.



